﻿using UnityEngine;
using System.Collections;

public class HangarMenu : MonoBehaviour
{
	//Menu Items
	public bool isExit;
	
	//Resources
	private int credits;
	private int salvage;
	public TextMesh CreditCount;
	public TextMesh SalvageCount;
	
	//Defense
	public bool isDefensePlus;
	public bool isDefenseMinus;
	public TextMesh DefenseStat;
	
	//Offense
	public bool isOffensePlus;
	public bool isOffenseMinus;
	public TextMesh OffenseStat;
	
	//Speed
	public bool isSpeedPlus;
	public bool isSpeedMinus;
	public TextMesh SpeedStat;
	
	//Capacity
	public bool isCapacityPlus;
	public bool isCapacityMinus;
	public TextMesh CapacityStat;

	//Consumables
	public bool isHull;
	public TextMesh HullStat;
	public bool isFuel;
	public TextMesh FuelStat;
	public bool isMisslePlus;
	public bool isMissleMinus;
	public TextMesh MissleStat;
	public bool isTorpedoPlus;
	public bool isTorpedoMinus;
	public TextMesh TorpedoStat;

	void Start()
	{
		CreditCount.text = PlayerStats.credits + "";
		SalvageCount.text = PlayerStats.salvage + "";

		DefenseStat.text = PlayerStats.defense + "";
		OffenseStat.text = PlayerStats.offense + "";
		SpeedStat.text = PlayerStats.speed + "";
		CapacityStat.text = PlayerStats.capacity + "";

		HullStat.text = PlayerStats.currentHullIntegrity + "";
		FuelStat.text = PlayerStats.currentFuelLevel + "";
		MissleStat.text = PlayerStats.currentMissles + "";
		TorpedoStat.text = PlayerStats.currentTorpedoes + "";
	}
	
	void OnMouseEnter()	{ renderer.material.color = Color.grey; }
	
	void OnMouseExit() { renderer.material.color = Color.white;	}
	
	void OnMouseDown()
	{
		if (isExit)
		{
			Application.LoadLevel("MainScene");
		}
		
		if (PlayerStats.credits > 0 && PlayerStats.salvage > 0)
		{
			if (isDefensePlus)
			{		
				PlayerStats.credits -= 1;
				PlayerStats.salvage -= 1;
				PlayerStats.defense += 1;

				PlayerStats.ModifyDefense();
				
				DefenseStat.text = PlayerStats.defense + "";
			}
			else if (isOffensePlus)
			{		
				PlayerStats.credits -= 1;
				PlayerStats.salvage -= 1;
				PlayerStats.offense += 1;

				PlayerStats.ModifyOffense();
								
				OffenseStat.text = PlayerStats.offense + "";
			}
			else if (isSpeedPlus)
			{		
				PlayerStats.credits -= 1;
				PlayerStats.salvage -= 1;
				PlayerStats.speed += 1;

				PlayerStats.ModifySpeed();

				SpeedStat.text = PlayerStats.speed + "";
			}
			else if (isCapacityPlus)
			{		
				PlayerStats.credits -= 1;
				PlayerStats.salvage -= 1;
				PlayerStats.capacity += 1;

				PlayerStats.ModifyCapacity();

				CapacityStat.text = PlayerStats.capacity + "";
			}
		}

		//Incrementing Consumables
		if (PlayerStats.credits > 0)
		{
			if (isHull)
			{
				PlayerStats.credits -= 1;
				PlayerStats.currentHullIntegrity += 10;
				HullStat.text = PlayerStats.currentHullIntegrity + "";
			}
			else if (isFuel)
			{
				PlayerStats.credits -= 1;
				PlayerStats.currentFuelLevel += 10;
				FuelStat.text = PlayerStats.currentFuelLevel + "";
			}
			else if (isMisslePlus)
			{
				PlayerStats.credits -=1;
				PlayerStats.currentMissles += 1;
				MissleStat.text = PlayerStats.currentMissles + "";
			}
			else if (isTorpedoPlus)
			{
				PlayerStats.credits -= 1;
				PlayerStats.currentTorpedoes += 1;
				TorpedoStat.text = PlayerStats.currentTorpedoes + "";
			}
		}

		//Decrementing Consumables
		{
			if (isMissleMinus)
			{
				PlayerStats.credits += 1;
				PlayerStats.currentMissles -= 1;

				MissleStat.text = PlayerStats.currentMissles + "";
			}
			else if (isTorpedoMinus)
			{
				PlayerStats.credits += 1;
				PlayerStats.currentTorpedoes -= 1;

				TorpedoStat.text = PlayerStats.currentTorpedoes + "";
			}
		}
		
		if (isDefenseMinus)
		{		
			PlayerStats.salvage += 1;
			PlayerStats.defense -=1;

			PlayerStats.ModifyDefense();

			DefenseStat.text = PlayerStats.defense + "";
		}
		else if (isOffenseMinus)
		{		
			PlayerStats.salvage += 1;
			PlayerStats.offense -=1;
			
			PlayerStats.ModifyOffense();
			
			OffenseStat.text = PlayerStats.offense + "";
		}
		else if (isSpeedMinus)
		{		
			PlayerStats.salvage += 1;
			PlayerStats.speed -=1;
			
			PlayerStats.ModifySpeed();
			
			SpeedStat.text = PlayerStats.speed + "";
		}
		else if (isCapacityMinus)
		{		
			PlayerStats.salvage += 1;
			PlayerStats.capacity -=1;
			
			PlayerStats.ModifyCapacity();
			
			CapacityStat.text = PlayerStats.capacity + "";
		}
		
		CreditCount.text = PlayerStats.credits + "";
		SalvageCount.text = PlayerStats.salvage + "";	
	}
	
	void OnMouseOver () 
	{
		if (Input.GetMouseButtonDown(1))
		{
			PlayerStats.credits += 1;
			PlayerStats.salvage += 1;
			
			CreditCount.text = PlayerStats.credits + "";
			SalvageCount.text = PlayerStats.salvage + "";
		}
	}
}