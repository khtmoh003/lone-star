﻿using UnityEngine;
using System.Collections;

public class PlayerDetection : MonoBehaviour
{
	//public GameObject player;																// Reference to the player.
	public static Vector3 lastPlayerSighting = new Vector3(100000f, 100000f, 100000f);		// Reference to the global last sighting of the player.
	
	void OnTriggerStay(Collider other)
	{
		// If the colliding gameobject is the player
		if(other.gameObject.tag == Tags.player)
		{
			lastPlayerSighting = other.gameObject.transform.position;

			// Raycast from the camera towards the player
			Vector3 relPlayerPos = other.gameObject.transform.position - transform.position;
			RaycastHit hit;

			Debug.DrawRay(this.transform.position, relPlayerPos);

			if(Physics.Raycast(transform.position, relPlayerPos, out hit))
			{	// If the raycast hits the player
				if(hit.collider.gameObject.tag == Tags.player)
				{	// Set the last global sighting of the player to the player's position
					lastPlayerSighting = other.gameObject.transform.position;
				}
			}
		}
	}

	void OnTriggerExit(Collider other)
	{
		if(other.gameObject.tag == Tags.player)
		{
			lastPlayerSighting = new Vector3(100000f, 100000f, 100000f); 
		}
	}
}