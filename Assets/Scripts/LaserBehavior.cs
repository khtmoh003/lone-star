﻿using UnityEngine;
using System.Collections;

public class LaserBehavior : MonoBehaviour
{
	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == Tags.awarenessBubble)
		{
			Debug.Log("Registered");
		}


		// If the colliding gameobject is the player...
		if(other.gameObject.tag == Tags.enemy)
		{
			Destroy(other);
			Destroy(this);
		}
	}
}