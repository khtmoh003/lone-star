﻿using UnityEngine;
using System.Collections;

public class Orbiter : MonoBehaviour
{
	public Transform center;
	public Vector3 axis;
	public float radius;
	public float radiusSpeed;
	public float rotationSpeed; 

	void Start()
	{
		transform.position = (transform.position - center.position).normalized * radius + center.position;
	}
	
	void Update()
	{
		transform.RotateAround (center.position, axis, rotationSpeed * Time.deltaTime);
		Vector3 desiredPosition = (transform.position - center.position).normalized * radius + center.position;
		transform.position = Vector3.MoveTowards(transform.position, desiredPosition, Time.deltaTime * radiusSpeed);

		transform.LookAt(transform.position + rigidbody.velocity);
	}
}
