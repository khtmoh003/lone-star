﻿using UnityEngine;
using System.Collections;

public class Tags : MonoBehaviour
{
	// A list of tag strings.
	public const string player = "Player";
	public const string enemy = "Enemy";
	public const string mainLight = "MainLight";
	public const string pilotLight = "PilotLight";
	public const string warningLight = "WarningLight";
	public const string gameController = "GameController";
	public const string awarenessBubble = "AwarenessBubble";
	public const string fader = "Fader";
}