﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour
{
	public float speed;
	public Transform[] waypoints;
	public bool patrolling = true;

	private int currentWaypoint;
	private Vector3 target;
	private Vector3 moveDirection;
	private Vector3 velocity;

	void Update()
	{
		/**Fight**/
		//if (){}


		/**Patrol**/
		if (currentWaypoint < waypoints.Length)
		{
			target = waypoints[currentWaypoint].position;
			moveDirection = target - transform.position;
			velocity = rigidbody.velocity;

			if(moveDirection.magnitude < 1)
			{
				currentWaypoint++;
			}
			else
			{
				velocity = moveDirection.normalized * speed;
			}
		}
		else
		{
			if (patrolling)
			{
				currentWaypoint = 0;
			}
			else
			{
				velocity = Vector3.zero;
			}
		}

		rigidbody.velocity = velocity;
		transform.LookAt(target);
	}
}
