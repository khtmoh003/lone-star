﻿using UnityEngine;
using System.Collections;

public class PlayerStats : MonoBehaviour
{
	//Resources
	public static int credits = 3;
	public static int salvage = 3;
	
	//Current Player Condition
	public static int currentHullIntegrity = 100;
	public static int currentShieldStrength = 100;
	public static int currentCapacitorLevel = 100;
	public static int currentFuelLevel = 100;
	public static int currentMissles = 0;
	public static int currentTorpedoes = 0;

	//Upgrades
	public static int defense = 0;
	public static int offense = 0;
	public static int speed = 0;
	public static int capacity = 0;
	
	/**Player Stats**/
	
	//Speed
	public static float forwardSpeed = 1500.0f;
	public static float acceleration = 25.0f;
	public static float afterburnerMultiplier = 2.0f;
	
	//Maneuverability
	public static int strafeSpeed = 30;
	public static int rotationSpeed = 45;
	public static int turnSpeed = 75;
	
	//Defensive
	private static int hullHealth = 500;
	private static int shieldHealth = 200;
	private static int shieldRechargeRate = 5;
	private static int shieldCost = 10;

	//Offense
	public static int laserDamage = 75;
	public static int laserSpeed = 5000;
	public static int laserCost = 5;
	public static double laserCooldown = 1;

	//Capacity
	public static int capacitorCapacity = 500;
	public static int fuelCapacity = 1000;
	public static int missleCapacity = 5;
	public static int torpedoCapacity = 0;

	/**Upgrades**/	
	public static void ModifyDefense()  // Defense
	{
		hullHealth = 500 + (defense * 100);
		shieldHealth = 200 + (defense * 50);
		shieldRechargeRate = 5 + (defense * 1);
	}	

	public static void ModifyOffense() // Offense
	{
		laserDamage = 75 + (offense * 25);
		laserCost = 50 + (offense * 15);
		laserCooldown = 1 - (offense * 0.075);
	}	

	public static void ModifySpeed() // Speed
	{
		acceleration = 5 + (speed * 1);
		forwardSpeed = 50000 + (speed * 10000);
		strafeSpeed = 30 + (capacity * 10);
	}	

	public static void ModifyCapacity() // Capacity
	{
		capacitorCapacity = 500 + (capacity * 100);
		fuelCapacity = 1000 + (capacity * 250);
		missleCapacity = 5 + (capacity * 2);
		torpedoCapacity = (capacity);
	}
}