﻿using UnityEngine;
using System.Collections;

public class Hangar : MonoBehaviour
{
	public GameObject hangar;
	public GameObject player;
	public GameObject menu;

	void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == Tags.player)
		{
			Application.LoadLevel("MainScene");
		}
	}
}
