﻿using UnityEngine;
using System.Collections;

public class HandleInCombat : MonoBehaviour
{
	/**Public Variables**/
	public static Vector3 position = new Vector3(100000f, 100000f, 100000f);			// The last global sighting of the player.
	public static Vector3 resetPosition = new Vector3(100000f, 100000f, 100000f);		// The default position if the player is not in sight.
	public AudioSource combatAudio;														// Reference to the AudioSource of the combat music.
	public Light pilotLight;															// Reference to the pilot light.
	public Light warningLight;															// Reference to the warning light.
	public float warningLightFadeSpeed = 2f;											// How fast the light fades between intensities.
	public float pilotLightFadeSpeed = 7f;												// How fast the light fades between low and high intensity.
	public float lightHighIntensity = 4f;												// The directional light's intensity when the alarms are off.
	public float lightLowIntensity = 0f;												// The directional light's intensity when the alarms are on.
	public float musicFadeSpeed = 1f;													// The speed at which the audio fades between the low and hight intensity.
	public float changeMargin = 0.2f;													// The margin within which the target intensity is changed.
	/**Private Variables**/
	private float targetIntensity;														// The intensity that the light is aiming for currently.

	void Awake()
	{
		targetIntensity = lightHighIntensity;
	}

	void Update ()
	{
		position = PlayerDetection.lastPlayerSighting;
		ToggleWarning();
		MusicFading();
	}
	
	
	void ToggleWarning ()
	{		
		// If the position is the reset position
		if (position == resetPosition)
		{	// Set the new intensity to low.
			pilotLight.intensity = Mathf.Lerp (pilotLight.intensity, lightHighIntensity, pilotLightFadeSpeed * Time.deltaTime);						
			// Lerp the light's intensity towards the current target.
			warningLight.intensity = Mathf.Lerp(warningLight.intensity, lightLowIntensity, warningLightFadeSpeed * Time.deltaTime);
		}
		else
		{	// Otherwise set the new intensity to high.
			pilotLight.intensity = Mathf.Lerp (pilotLight.intensity, lightLowIntensity, pilotLightFadeSpeed * Time.deltaTime);
			// Otherwise fade the light's intensity to zero.
			warningLight.intensity = Mathf.Lerp(warningLight.intensity, targetIntensity, warningLightFadeSpeed * Time.deltaTime);

			// Check whether the target intensity needs changing and change it if so.
			CheckTargetIntensity();
		}
	}

	void CheckTargetIntensity ()
	{
		// If the difference between the target and current intensities is less than the change margin
		if(Mathf.Abs(targetIntensity - warningLight.intensity) < changeMargin)
		{
			// If the target intensity is high
			if(targetIntensity == lightHighIntensity)
				// Then set the target to low
				targetIntensity = lightLowIntensity;
			else
				// Otherwise set the targer to high
				targetIntensity = lightHighIntensity;
		}
	}	
	
	void MusicFading ()
	{
		// If the alarm is not being triggered
		if(position != resetPosition)
		{
			// Fade out the normal music
			audio.volume = Mathf.Lerp(audio.volume, 0f, musicFadeSpeed * Time.deltaTime);
			
			// Fade in the panic music
			combatAudio.volume = Mathf.Lerp(combatAudio.volume, 0.8f, musicFadeSpeed * Time.deltaTime);
		}
		else
		{
			// Otherwise fade in the normal music and fade out the panic music.
			audio.volume = Mathf.Lerp(audio.volume, 0.8f, musicFadeSpeed * Time.deltaTime);
			combatAudio.volume = Mathf.Lerp(combatAudio.volume, 0f, musicFadeSpeed * Time.deltaTime);
		}
	}
}