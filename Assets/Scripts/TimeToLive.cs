﻿using UnityEngine;
using System.Collections;

public class TimeToLive : MonoBehaviour {

	public int timeToLive = 10;

	void Start ()
	{	
		Destroy (gameObject, timeToLive);	// Time to Live in seconds

	}
}
