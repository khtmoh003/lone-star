﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
	//Mouse Movement
	public float sensitivityX = 15.0f;
	public float sensitivityY = 15.0f;
	private float rotationX = 0.0f;
	private float rotationY = 0.0f;
	private Quaternion originalRotation;

	//HUD
	public TextMesh energyDisplay;
	public TextMesh speedDisplay;
	
	//Spawning the Laser Correctly
	public Transform laserPosition;
	private Vector3 laserLocation;
	
	//Prefabs
	public Rigidbody laserPrefab;
	
	//Ship State
	private float currentSpeed = 0.0f;
	private double cooldown = 0;
	private bool isLaser = true;
	private bool isMissles = false;
	private bool isTorpedoes = false;
	private bool afterburnerEngaged = false;
	private bool deceleratingToZero = false;
	
	void Start()
	{
		originalRotation = transform.localRotation;
	}
	
	void Update()
	{
		//Set DeceleratingToZero to False
		deceleratingToZero = false;

		// Read the mouse input axis
		rotationX += Input.GetAxis("Mouse X") * sensitivityX;
		rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
		
		Quaternion xQuaternion = Quaternion.AngleAxis (rotationX, Vector3.up);
		Quaternion yQuaternion = Quaternion.AngleAxis (rotationY, Vector3.left);
		
		transform.localRotation = originalRotation * xQuaternion * yQuaternion;
		
		//Keyboard Movement
		if (Input.GetKey (KeyCode.W)) { transform.Translate(Vector3.up * PlayerStats.strafeSpeed * Time.deltaTime); }
		if (Input.GetKey (KeyCode.S)) { transform.Translate(Vector3.down * PlayerStats.strafeSpeed * Time.deltaTime); }
		if (Input.GetKey (KeyCode.A)) { transform.Translate(Vector3.left * PlayerStats.strafeSpeed * Time.deltaTime); }
		if (Input.GetKey (KeyCode.D)) { transform.Translate(Vector3.right * PlayerStats.strafeSpeed * Time.deltaTime); }
		
		//Shooting
		if (Input.GetKey(KeyCode.Mouse0) && Time.time >= cooldown && !afterburnerEngaged) { Fire(); }
		
		//Scanning
		//if (Input.GetButton("Fire2")) { /*Scan();*/ }

		//Cycle Between Enemies
		if(Input.GetKey(KeyCode.LeftAlt))
		{
			//Previous Enemy
		}
		if (Input.GetKey (KeyCode.LeftControl))
		{
			//Next Enenmy
		}
		
		//Cycle Between Weapons
		if (Input.GetKey (KeyCode.Alpha1))
		{
			isLaser = true;
			isMissles = false;
			isTorpedoes = false;
		}
		else if (Input.GetKey (KeyCode.Alpha2))
		{
			isLaser = false;
			isMissles = true;
			isTorpedoes = false;
		}
		else if (Input.GetKey (KeyCode.Alpha3))
		{
			isLaser = false;
			isMissles = false;
			isTorpedoes = true;
		}
		
		//Forward movement		
		if (Input.GetKey(KeyCode.Q))
		{
			if ((currentSpeed + PlayerStats.acceleration) > PlayerStats.forwardSpeed)
			{
				currentSpeed = PlayerStats.forwardSpeed;
			}
			else if (currentSpeed <= PlayerStats.forwardSpeed)
			{
				currentSpeed += PlayerStats.acceleration;
			}
		}
		
		if (Input.GetKey(KeyCode.E))
		{
			if ((currentSpeed - PlayerStats.acceleration) < -PlayerStats.forwardSpeed)
			{
				currentSpeed = -PlayerStats.forwardSpeed;
			}
			else if (currentSpeed >= -PlayerStats.forwardSpeed)
			{
				currentSpeed -= PlayerStats.acceleration;
			}
		}

		if (Input.GetKey(KeyCode.C))
	    {
			deceleratingToZero = true;

			if (Mathf.Abs(currentSpeed) == 25)
			{
				currentSpeed = 0;
			}
			else if (currentSpeed > 0)
			{
				currentSpeed -= PlayerStats.acceleration * 2;
			}
			else if (currentSpeed < 0)
			{
				currentSpeed += PlayerStats.acceleration * 2;
			}
		}
		
		if (Input.GetKey(KeyCode.LeftShift) && !deceleratingToZero)
		{
			afterburnerEngaged = true;

			rigidbody.velocity = (transform.forward * currentSpeed * PlayerStats.afterburnerMultiplier * Time.deltaTime);
			speedDisplay.text = "Speed: " + (currentSpeed * PlayerStats.afterburnerMultiplier); 
		}
		else
		{
			afterburnerEngaged = false;

			rigidbody.velocity = (transform.forward * currentSpeed * Time.deltaTime);

			speedDisplay.text = "Speed: " + currentSpeed;
		}

		energyDisplay.text = "Energy: " + PlayerStats.currentCapacitorLevel;
	}
	
	private void Fire()
	{
		if (isLaser)
		{
			laserLocation.x = laserPosition.position.x;
			laserLocation.y = laserPosition.position.y;
			laserLocation.z = laserPosition.position.z;
			
			Rigidbody laser1 = Instantiate(laserPrefab, laserLocation, laserPosition.rotation) as Rigidbody;
			laser1.rigidbody.AddForce(laserPosition.forward * PlayerStats.laserSpeed);
			laserPosition.DetachChildren();

			PlayerStats.currentCapacitorLevel -= PlayerStats.laserCost;
		}
		
		cooldown = Time.time + PlayerStats.laserCooldown;
	}
}